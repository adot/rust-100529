pub enum ParseError {
    UnexpectedEndTag(#[doc(hidden)] String),
}
