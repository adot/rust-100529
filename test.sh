#!/bin/bash
set -eou pipefail
cargo +nightly rustdoc --lib -- -Z unstable-options --output-format json
